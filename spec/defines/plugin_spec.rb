# frozen_string_literal: true

require 'spec_helper'

describe 'check_mk_agent::plugin' do
  let(:title) { 'namevar' }
  let(:pre_condition) { 'include check_mk_agent' }

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      context 'default params' do
        let(:params) do
          {}
        end
        it { is_expected.to compile }
        it { is_expected.to contain_file('/usr/lib/check_mk_agent/plugins/namevar').
                              with('ensure' => 'present',
                                   'owner'  => 'root',
                                   'group'  => 0,
                                   'mode'   => '0750',
                                   'source' =>
                                   [
                                     "puppet:///modules/check_mk_agent/plugins/v0/namevar.linux",
                                     "puppet:///modules/check_mk_agent/plugins/v0/namevar",
                                     "puppet:///modules/check_mk_agent/plugins/namevar.linux",
                                     "puppet:///modules/check_mk_agent/plugins/namevar",
                                   ], 
                                  ) }
        it { is_expected.to contain_exec('/bin/rm -f /usr/lib/check_mk_agent/plugins/*/namevar').with('refreshonly' => true) }

      end
      context 'set seconds' do
        let(:params) do
          { 'content' => "forking" }
        end
        it { is_expected.to compile }
        it { is_expected.to contain_file('/usr/lib/check_mk_agent/plugins/namevar').
                              with('ensure'  => 'present',
                                   'owner'   => 'root',
                                   'group'   => 0,
                                   'mode'    => '0750',
                                   'content' => "forking",
                                  ) }
      end
      context 'set seconds' do
        let(:params) do
          { 'seconds' => 300 }
        end
        it { is_expected.to compile }
        it { is_expected.to contain_file('/usr/lib/check_mk_agent/plugins/300/namevar').
                              with('ensure' => 'present',
                                   'owner'  => 'root',
                                   'group'  => 0,
                                   'mode'   => '0750',
                                   'source' =>
                                   [
                                     "puppet:///modules/check_mk_agent/plugins/v0/namevar.linux",
                                     "puppet:///modules/check_mk_agent/plugins/v0/namevar",
                                     "puppet:///modules/check_mk_agent/plugins/namevar.linux",
                                     "puppet:///modules/check_mk_agent/plugins/namevar",
                                   ], 
                                  ) }
        it { is_expected.to contain_file('/usr/lib/check_mk_agent/plugins/300').
                              with('ensure' => 'directory',
                                   'owner'  => 'root',
                                   'group'  => 0,
                                   'mode'   => '0750',
                                  ) }
        it { is_expected.to contain_file('/usr/lib/check_mk_agent/plugins/namevar').
                              with('ensure' => 'absent'
                                  ) }
      end
    end
  end
end
