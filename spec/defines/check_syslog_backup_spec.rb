# frozen_string_literal: true

require 'spec_helper'

describe 'check_mk_agent::check_syslog_backup' do
  let(:title) { 'namevar' }
  let(:pre_condition) { 'include check_mk_agent' }
  let(:params) do
    {}
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
      it { is_expected.to contain_file('/usr/lib/check_mk_agent/local/namevar').
                            with('ensure' => 'present',
                                 'owner'  => 'root',
                                 'group'  => 0,
                                 'mode'   => '0750',
                                 'source' => 'puppet:///modules/check_mk_agent/local/check_syslog_backup',
                                ) }
    end
  end
end
