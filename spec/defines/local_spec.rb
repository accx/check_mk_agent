# frozen_string_literal: true

require 'spec_helper'

describe 'check_mk_agent::local' do
  let(:title) { 'namevar' }
  let(:pre_condition) { 'include check_mk_agent' }

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
      context 'default params' do
        let(:params) do
          {}
        end
        it { is_expected.to contain_file('/usr/lib/check_mk_agent/local/namevar').
                              with('ensure' => 'present',
                                   'owner'  => 'root',
                                   'group'  => 0,
                                   'mode'   => '0750',
                                   'source' => ['puppet:///modules/check_mk_agent/local/namevar.foo',
                                                'puppet:///modules/check_mk_agent/local/namevar'], 
                                  ) }
      end
      context 'default params' do
        let(:params) do
          { 'seconds' => 3600 }
        end
        it { is_expected.to contain_file('/usr/lib/check_mk_agent/local/namevar').with('ensure' => 'absent') }
        it { is_expected.to contain_file('/usr/lib/check_mk_agent/local/3600/namevar').
                              with('ensure' => 'present',
                                  ) }
        it { is_expected.to contain_file('/usr/lib/check_mk_agent/local/3600').
                              with('ensure' => 'directory',
                                  ) }
      end
    end
  end
end
