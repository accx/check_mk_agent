require 'spec_helper'

describe 'check_mk_agent::install' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:pre_condition) { 'include check_mk_agent' }
      it { is_expected.to compile }
    end
  end
end
