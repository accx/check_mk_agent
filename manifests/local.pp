# @summary install a local cmk check
#
# install a local check in cmk, optionally using a cache time of
# seconds
#
# @param ensure
#   install or remove the local check
# @param seconds
#   use a cache time of seconds for this local check
# @param source
#   use this source for the local check
# @example
#   check_mk_agent::local { 'some_local':
#     seconds => '1200',
#   }
#
define check_mk_agent::local (
  Enum['present','absent'] $ensure = 'present',
  Integer $seconds = 0,
  Variant[String,Array[String]] $source = [
    "puppet:///modules/check_mk_agent/local/${title}.${facts['networking']['hostname']}",
    "puppet:///modules/check_mk_agent/local/${title}",
  ],
) {
  if($seconds > 0) {
    ensure_resource(
      'file',
      "${check_mk_agent::mk_libdir}/local/${seconds}",
      {
        'ensure' => 'directory',
        owner    => 'root',
        group    => 0,
        'mode'   => '0750',
        require  => File["${check_mk_agent::mk_libdir}/local"],
      }
    )
    # remove the old file directly in local
    file { "${check_mk_agent::mk_libdir}/local/${title}":
      ensure => absent,
    }
    # the path includes the seconds
    $path="${check_mk_agent::mk_libdir}/local/${seconds}/${title}"
    $dir="${check_mk_agent::mk_libdir}/local/${seconds}"
    $cache_path = "${check_mk_agent::mk_vardir}/cache/local_${seconds}\\${title}.cache"
  }
  else {
    $path="${check_mk_agent::mk_libdir}/local/${title}"
    $dir="${check_mk_agent::mk_libdir}/local"
    $cache_path = "${check_mk_agent::mk_vardir}/cache/${title}.cache"
  }
  @file { $path:
    ensure  => $ensure,
    owner   => 'root',
    group   => 0,
    mode    => '0750',
    source  => $source,
    require => File[$dir],
    tag     => 'check_mk_agent_local',
  }
  if($ensure == 'absent') {
    file { $cache_path:
      ensure => absent,
    }
  }
}
