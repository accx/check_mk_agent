# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include check_mk_agent::install
class check_mk_agent::install {
  $downcase_kernel=downcase($facts['kernel'])
  package { $check_mk_agent::agent_package:
    ensure => $check_mk_agent::package_ensure,
  }
  file {
    [
      $check_mk_agent::mk_vardir,
      $check_mk_agent::mk_confdir,
      $check_mk_agent::mk_libdir,
      "${check_mk_agent::mk_vardir}/job",
      "${check_mk_agent::mk_vardir}/spool",
      "${check_mk_agent::mk_vardir}/cache",
    ]:
      ensure  => directory,
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
      require => Package[$check_mk_agent::agent_package],
  }
  # purge these directories of non-managed files
  file {
    [
      "${check_mk_agent::mk_libdir}/local",
      "${check_mk_agent::mk_libdir}/plugins",
    ]:
      ensure  => directory,
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
      purge   => true,
      recurse => true,
      require => Package[$check_mk_agent::agent_package],
  }
  $check_mk_agent::executables.each |String $exec| {
    file { "${check_mk_agent::bindir}/${exec}":
      owner   => 'root',
      group   => 0,
      mode    => '0755',
      source  => [
        "puppet:///modules/check_mk_agent/${exec}.${downcase_kernel}.v${check_mk_agent::version}",
        "puppet:///modules/check_mk_agent/${exec}.v${check_mk_agent::version}",
        "puppet:///modules/check_mk_agent/${exec}.${downcase_kernel}",
        "puppet:///modules/check_mk_agent/${exec}",
      ],
      require => Package[$check_mk_agent::agent_package],
    }
  }
  # I believe this should be titled stale not state, but do not change it
  cron { 'remove_state_check_mk_cache_files':
    command => "test -d ${check_mk_agent::mk_vardir}/cache && find ${check_mk_agent::mk_vardir}/cache -type f -name \"*.cache.new\" -mmin +7200 -delete",
    user    => 'root',
    minute  => '7',
  }
}
