# @summary configure xinetd for check_mk
#
# use xinetd module to configure the service
#
# @example
#   include check_mk_agent
class check_mk_agent::service (
) {
  if( $check_mk_agent::service_provider == 'systemd' ) {
    # in case it was installed previously, turn off this junk
    service { 'cmk-agent-ctl-daemon':
      ensure => 'stopped',
      enable => false,
    }
    $use_async = $check_mk_agent::version ? {
      /^[01]/ => false,
      default => true,
    }
    systemd::unit_file { 'check-mk-agent-async.service':
      active => $use_async,
      enable => $use_async,
      source => 'puppet:///modules/check_mk_agent/systemd/check-mk-agent-async.service',
    }
    systemd::unit_file { 'check-mk-agent.socket':
      source => 'puppet:///modules/check_mk_agent/systemd/check-mk-agent.socket.fallback',
      enable => true,
      active => true,
    }
    systemd::unit_file { 'check-mk-agent@.service':
      source => 'puppet:///modules/check_mk_agent/systemd/check-mk-agent@.service',
    }
    # manage xinetd without using xinetd module:
    file { ['/etc/xinetd.d/check_mk','/etc/xinetd.d/check_mk_agent']:
      ensure => absent,
      notify => Exec['pkill -HUP xinetd'],
    }
    exec { 'pkill -HUP xinetd':
      path        => ['/bin','/usr/bin'],
      refreshonly => true,
      before      => Systemd::Unit_file['check-mk-agent.socket'],
    }
  }
  elsif ( $check_mk_agent::service_provider == 'xinetd' ) {
    create_resources('xinetd::service',$check_mk_agent::xinetd_services, { 'ensure' => 'present' })
    # just in case we want xinetd on a systemd system, let's disable all the services known to be installed by the package:
    service { ['cmk-agent-ctl-daemon','check-mk-agent-async','check-mk-agent.socket']:
      ensure => 'stopped',
      enable => false,
    }
  }
}
