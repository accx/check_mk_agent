# @summary check_mk_agent::plugin
#
# place a check_mk plugin file into the defined plugin directory
#
# @param ensure
#   install the plugin if present, remove it if absent
# @param seconds
#   plugins have a caching mechanism, and setting the seconds causes
#   the agent to run the plugin only ever X seconds
# @param link
#   unused option, probably leftover from one known plugins were in agent
# @param variant
#   cmk ships plugin variants for operating systems, you probably don't need to change this
# @param content
#   if set, use this as the content for the plugin itself, otherwise
#   use the source parameter
# @param source
#   get the plugin from this source
# @example
#   check_mk_agent::plugin { 'pluggy':
#     seconds => 200,
#     content => "#! /bin/bash\nls -l\n",
#   }
define check_mk_agent::plugin (
  Enum['present','absent'] $ensure = 'present',
  Integer $seconds = 0,
  Boolean $link = false,
  String $variant = downcase($facts['kernel']),
  Variant[String,Undef] $content = undef,
  Variant[String,Array[String]] $source = [
    "puppet:///modules/check_mk_agent/plugins/v${check_mk_agent::version}/${title}.${variant}",
    "puppet:///modules/check_mk_agent/plugins/v${check_mk_agent::version}/${title}",
    "puppet:///modules/check_mk_agent/plugins/${title}.${variant}",
    "puppet:///modules/check_mk_agent/plugins/${title}",
  ],
) {
  if($seconds > 0) {
    ensure_resource(
      'file',
      "${check_mk_agent::mk_libdir}/plugins/${seconds}",
      { 'ensure' => 'directory', owner => 'root', group => 0, 'mode' => '0750' }
    )
    # remove the old file directly in local
    file { "${check_mk_agent::mk_libdir}/plugins/${title}":
      ensure => absent,
    }
    # the path includes the seconds
    $path="${check_mk_agent::mk_libdir}/plugins/${seconds}/${title}"
    $cache_path = "${check_mk_agent::mk_vardir}/cache/plugins_${seconds}\\${title}.cache"
  }
  else {
    $path="${check_mk_agent::mk_libdir}/plugins/${title}"
    # when the plugin is updated, nuke versions hidden under plugins
    exec { "/bin/rm -f ${check_mk_agent::mk_libdir}/plugins/*/${title}":
      refreshonly => true,
      subscribe   => File[$path],
    }
    $cache_path = "${check_mk_agent::mk_vardir}/cache/${title}.cache"
  }
  if($content) {
    @file { $path:
      ensure  => $ensure,
      owner   => 'root',
      group   => 0,
      mode    => '0750',
      content => $content,
      tag     => 'check_mk_agent_plugin',
    }
  }
  else {
    @file { $path:
      ensure => $ensure,
      owner  => 'root',
      group  => 0,
      mode   => '0750',
      source => $source,
      tag    => 'check_mk_agent_plugin',
    }
  }
  if($ensure == 'absent') {
    file { $cache_path:
      ensure => absent,
    }
  }
}
