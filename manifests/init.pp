# @summary install and enable the check_mk_agent
#
# install check_mk_agent thanks
#
# @param manage
#   when false, do nothing
# @param xinetd_services
#   xinetd services, passed to the xinetd module
# @param service_provider
#   use xinetd or systemd?
# @param mk_libdir
#   set to the MK_LIBDIR environment variable as baked into the agent
#   used to locate the plugins, local, etc. directories
# @param mk_confdir
#   set to the MK_CONFDIR environment variable for config files, etc.
# @param mk_vardir
#   set to the MK_VARDIR environment variable as baked into the agent
#   used for findind the location to clean old cache files
# @param bindir
#   location to put the executables
# @param executables
#   which exeutables to put into bindir
# @param package_ensure
#   remove or install the package
# @param agent_package
#   name of the package to install, per the package_ensure parameter
# @param csgchecks
#   configuration file for local checks in the new format
# @param old_style_csgchecks
#   unconverted csg local checks
# @param locals
#   create these local check files, passed to the check_mk_agent::local defined type
# @param plugins
#   create these plugin check files, passed to the check_mk_agent::plugin defined type
# @param puppet_labels
#   create these cmk labels from puppet facts
# @param version
#   use this version string to find agents, plugins, etc. as installed from the puppet module itself
#   does not affect the version of the agent package installed
# @param logwatch_cfg
#   create a logwatch.cfg file from this data structure
#   see the logwatch plugin documentation and example
# 
# @example
#   include check_mk_agent
class check_mk_agent (
  Boolean $manage = true,
  Hash $xinetd_services = {
    'check_mk' => {
      'ensure' => 'absent',
      'server' => '/usr/bin/check_mk_agent',
    },
    'check_mk_agent' => {
      service_type => 'UNLISTED',
      port         => '6556',
      socket_type  => 'stream',
      protocol     => 'tcp',
      wait         => 'no',
      user         => 'root',
      server       => '/usr/bin/check_mk_caching_agent',
      disable      => 'no',
    },
  },
  Enum['xinetd','systemd'] $service_provider = 'systemd',
  String $mk_libdir = '/usr/lib/check_mk_agent',
  String $mk_confdir = '/etc/check_mk',
  String $mk_vardir = '/var/lib/check_mk_agent',
  String $bindir = '/usr/bin',
  Array[String] $executables = [
    'check_mk_agent',
    'check_mk_caching_agent',
    'waitmax',
    'mk-job',
  ],
  String $package_ensure = 'absent',
  String $agent_package = 'check-mk-agent',
  Hash $csgchecks = {},
  Hash $old_style_csgchecks = {},
  Hash $locals = {},
  Hash $plugins = {},
  Hash $puppet_labels = {
    'puppet/operatingsystem'           => $facts['os']['name'],
    'puppet/osfamily'                  => $facts['os']['family'],
    'puppet/operatingsystemmajrelease' => $facts['os']['release']['major'],
    'puppet/operatingsystemrelease'    => $facts['os']['release']['full'],
    'puppet/os_maj'                    => "${facts['os']['name']}_${facts['os']['release']['major']}",
    'puppet/osfamily_maj'              => "${facts['os']['family']}_${facts['os']['release']['major']}",
    'puppet/domain'                    => $facts['networking']['domain'],
  },
  String $version = '0',
  Hash[String,Array[Array[String,2,2]]] $logwatch_cfg = {
    '/var/log/messages' => [['W','error']],
  },
) {
  if( $service_provider == 'xinetd' ) {
    include xinetd
  } elsif( $service_provider == 'systemd' ) {
    include systemd
  }
  include stdlib
  contain check_mk_agent::install
  contain check_mk_agent::config
  contain check_mk_agent::service
  if($manage == true) {
    Class['check_mk_agent::install']
    -> Class['check_mk_agent::config']
    -> Class['check_mk_agent::service']
  }
}
