# @summary install the check_syslog_backup check using some other name
#
# check_syslog_backup.pp for check_mk::agent::check_syslog_backup, I don't know what or why this does anything but I didn't write it so who cares
# @param ensure - present or absent
#
# @example
#   check_mk_agent::check_syslog_backup { 'DBsomething_or_over':
#   }
#
define check_mk_agent::check_syslog_backup ( Enum['present','absent'] $ensure = present ) {
  file { "${check_mk_agent::mk_libdir}/local/${title}":
    ensure => $ensure,
    owner  => 'root',
    group  => 0,
    mode   => '0750',
    source => 'puppet:///modules/check_mk_agent/local/check_syslog_backup',
  }
}
