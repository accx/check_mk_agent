# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include check_mk_agent::config
class check_mk_agent::config {
  file { "${check_mk_agent::mk_confdir}/csgchecks.ini":
    owner   => 'root',
    group   => 0,
    mode    => '0644',
    content => to_toml(deep_merge({ 'csgchecks' => $check_mk_agent::csgchecks },$check_mk_agent::old_style_csgchecks)),
  }
  file { "${check_mk_agent::mk_confdir}/csgchecks.yaml":
    owner   => 'root',
    group   => 0,
    mode    => '0644',
    content => to_yaml(deep_merge({ 'csgchecks' => $check_mk_agent::csgchecks },$check_mk_agent::old_style_csgchecks)),
  }
  file { "${check_mk_agent::mk_confdir}/csgchecks.json":
    owner   => 'root',
    group   => 0,
    mode    => '0644',
    content => to_json_pretty(deep_merge({ 'csgchecks' => $check_mk_agent::csgchecks },$check_mk_agent::old_style_csgchecks)),
  }
  ensure_resources('check_mk_agent::local',$check_mk_agent::locals)
  File<| tag == 'check_mk_agent_local' |>
  ensure_resources('check_mk_agent::plugin',$check_mk_agent::plugins)
  File<| tag == 'check_mk_agent_plugin' |>
  $json_file = "${check_mk_agent::mk_confdir}/puppet-tags.json"
  check_mk_agent::plugin { 'puppet_tags':
    content => template('check_mk_agent/puppet_tags.erb'),
  }
  $logwatch_cfg="${check_mk_agent::mk_confdir}/logwatch.cfg"
  file { $logwatch_cfg:
    owner     => 'root',
    group     => 0,
    mode      => '0444',
    content   => template('check_mk_agent/logwatch.cfg.erb'),
    show_diff => false,
  }
  # create a json struct to be spit out by the puppet_tags plugin
  file { $json_file:
    owner   => 'root',
    group   => 0,
    mode    => '0644',
    content => to_json($check_mk_agent::puppet_labels),
  }
}
