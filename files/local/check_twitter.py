#!/bin/python
# Program: twitterServiceHelper.py
# Purpose: To enable and disable services for the twitter-client
# Author:  Dan Holstad

# Import modules
import ConfigParser, re, os, fnmatch, getopt, sys, commands, string, fnmatch, time, smtplib
from email.mime.text import MIMEText

class twitterServiceHelper:
  """manage Twitter Data services"""

  def __init__ (self):
    """Set up the class"""
    self.serviceName           = ""
    self.serviceDirectory      = "/usr/lib/systemd/system/"
    self.ActiveDirectory       = "/etc/systemd/system/multi-user.target.wants/"
    self.serviceList           = {}
    self.action                = "nagios"
    
    # Read in the options
    self.read_Opts()
    try:
       rpt = getattr(self,"do_" + self.action)
       rpt()
    except 'stuff':
       self.error()

  def do_nagios(self):
    """Print nagios alert messages"""
    alerts = []
    for service in os.listdir('/twitter/etc/'):
      if not os.path.exists('/twitter/data/'+ service +'/.DoNotMonitor'):
        id,state,substate = self.getServiceStatus('twitter-'+service + '.service')
        newest = 99999999999
        try:
          for fl in os.listdir('/twitter/data/'+ service):
            if fnmatch.fnmatch(fl, '*.csv'):
              rtn=os.stat('/twitter/data/'+ service + '/'+fl)
              age = (time.time() - rtn.st_mtime)/3600
              if age <= newest:
                newest = age
        except:
          pass

        if newest > 1 and state != 'disabled':
          alerts.append(string.join([id,state,substate,"{:10.2f} hours".format(newest)],' '))

    if len(alerts) == 0:
      print "0 twitter-feeds - all feeds are running and updating"
    else:
      print "2 twitter-feeds - Stale feeds: " + string.join(alerts,',')
 
  def getServiceStatus(self,service):
    """Get service details of service"""
    outpt = commands.getoutput('systemctl show '+service)
    stats = {}
    for field in outpt.split('\n'):
      key,val = field.split('=',1)
      stats[key] = val
    return(stats['Id'],stats.get('UnitFileState','missing'),stats['SubState'])

  def error(self,errmsg=""):
     """Print usage"""
     if errmsg != "":
       print "**",errmsg
       print
     print """usage: twitterServiceHelper.py <action> [service name]
  supported actions:
    list            - list defined services
    create <name>   - create a new service
    enable <name>   - enable a service
    disable <name>  - disable a service
    start <name>    - start a service
    stop  <name>    - stop a service
    restart <name>  - restart a service
    status <name>   - status of a service

"""
     sys.exit(1)

  def read_Opts(self):
     try:
         opts, args = getopt.getopt(sys.argv[1:], "hvg:r:", ["help","verbose"])
     except getopt.GetoptError as err:
         # print help information and exit:
         print str(err) # will print something like "option -a not recognized"
         self.usage()
         sys.exit(2)
     output = None
     for o, a in opts:
         if o in  ("-v", "--verbose"):
             self.verbose = True
         elif o in ("-h", "--help"):
             self.usage()
             sys.exit()
         else:
             assert False, "unhandled option"  
   
     # if len(args) < 1:
     #   self.error(errmsg="No action specified")
     # if str(args[0]) in ['create','enable','disable','start','stop','restart','list','status','emailAlert']:
     #   self.action = args[0]
     # else:
     #   self.error(errmsg="Invalid action requested: "+args[0])
     
     # if len(args) >= 2:
     #   self.serviceName = args[1]
     # else:
     #   if self.action not in ['list','emailAlert']:
     #     self.error(errmsg="Service name is required for action: "+self.action) 

if __name__ == "__main__":
  prog = twitterServiceHelper()
