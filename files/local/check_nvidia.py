#!/usr/local/bin/mk_python

import os, fnmatch, re, time, datetime, getopt, sys, string
import commands, xml, subprocess
try:
  from elementtree import ElementTree
except:
  from xml.etree import ElementTree

rtn,outpt = commands.getstatusoutput('/usr/bin/nvidia-smi -q -x')
mytree = ElementTree.fromstring(outpt)
for gpu in mytree.findall('gpu'):
  id       = gpu.attrib['id']
  make     = gpu.find('product_name').text
  cpu_util = gpu.find('utilization/gpu_util').text[:-2] 
  mem_util = gpu.find('utilization/memory_util').text[:-2]
  temp     = gpu.find('temperature/gpu_temp').text[:-2]
  temp_wrn = gpu.find('temperature/gpu_temp_slow_threshold').text[:-2]
  temp_crt = gpu.find('temperature/gpu_temp_max_threshold').text[:-2] 
  pwr_draw = gpu.find('power_readings/power_draw').text[:-2]
  #  pwr_warn = gpu.find('power_readings/min_power_limit').text[:-2]
  #  pwr_crit = gpu.find('power_readings/max_power_limit').text[:-2]
  max_pwr  = float(gpu.find('power_readings/max_power_limit').text[:-2])
  pwr_warn = str(max_pwr*.90)
  pwr_crit = str(max_pwr*.95)
  print "P NVIDIA_"+id+" cpu_util="+cpu_util+";99;100|mem_util="+mem_util+";99;100|temp="+temp+";"+temp_wrn+";"+temp_crt+"|power="+pwr_draw+";"+pwr_warn+";"+pwr_crit+" Model: "+make
