#!/usr/local/bin/mk_python
import string, commands

class checkDocker():
  def __init__(self):
    """Set up the class"""
    self.columns = ['CONTAINER ID','IMAGE','COMMAND','CREATED','STATUS','PORTS','NAMES']
    self.headers = {}
    self.containers = {}
    self.errlist = []
    self.getStatus()
    self.report()

  def getStatus(self):
    "Read in the docker ps and parse content"
    rtn,outpt = commands.getstatusoutput('docker ps -a')
    # print outpt
    if rtn !=0:
		self.errorlist.append('Unknown error: ' + str(outpt))
    else:
      lines = outpt.split('\n')
      # figure out headers
      for col in range(len(self.columns)):
        if col == 0:
          self.headers[self.columns[col]] = {'start': 0}
        else:
          loc = lines[0].index(self.columns[col])
          self.headers[self.columns[col]] = {'start': loc}
          self.headers[self.columns[col -1]]['end'] =  loc -1
    for ln in lines[1:]:
      id     = 'DOCKER_'+string.strip(ln[self.headers['CONTAINER ID'].get('start',0):self.headers['CONTAINER ID'].get('end',len(ln))])
      status = string.strip(ln[self.headers['STATUS'].get('start',0):self.headers['STATUS'].get('end',len(ln))])
      name   = string.strip(ln[self.headers['NAMES'].get('start',0):self.headers['NAMES'].get('end',len(ln))])
      if status[0:2] == 'Up':
        self.errlist.append("0 %s - OK - %s %s" % (id,name,status))
      else:
        self.errlist.append("2 %s - CRITICAL - %s %s" % (id,name,status))
  
  def report(self):
   """print report"""
   for err in self.errlist:
     print err

if __name__ == "__main__":
  cls = checkDocker()
