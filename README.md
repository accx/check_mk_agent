# check_mk_agent

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with check_mk_agent](#setup)
    * [Setup requirements](#setup-requirements)
    * [Beginning with check_mk_agent](#beginning-with-check_mk_agent)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)

## Description

Install the check_mk agent, and configure related plugins and local checks.  Also configures xinetd and/or systemd services to allow access to the agent from the network.  Does not configure related firewall rules for allowing access.

## Setup

### Setup Requirements

Module expects that a package containing the check_mk agent is available via the package distribution system for your operating system, if you are using pacakges.

### Beginning with check_mk_agent

Including the module will give you some version of the agent.

## Usage

    woof


## Limitations

Trapped in a limbo between providing all the agent scripts and installing via a package.

## Development

Pull requests would be fine, I suppose

